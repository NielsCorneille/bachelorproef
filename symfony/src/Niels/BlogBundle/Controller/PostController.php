<?php

namespace Niels\BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Niels\BlogBundle\Entity\Post;
use Niels\BlogBundle\Entity\Tag;
use Niels\BlogBundle\Entity\Comment;
use Niels\BlogBundle\Form\CommentType;
use Niels\BlogBundle\Form\PostType;

/**
 * Post controller.
 *
 */
class PostController extends Controller
{
	/**
	 * Lists all Post entities.
	 *
	 */
	public function indexAction()
	{
		$em         = $this->getDoctrine()->getManager();
		$repository = $em->getRepository('BlogBundle:Post');
		$query      = $repository->createQueryBuilder('p')
			->where('p.status = ' . Post::STATUS_PUBLISHED)
			->orderBy('p.updateTime', 'DESC')
			->getQuery();

		$entities = $query->getResult();

		foreach ($entities as $entity)
		{
			$entity->tagLinks = array();
			foreach (Tag::string2array($entity->getTags()) as $tag)
			{
				$entity->tagLinks[] = array(
					'tag'   => $tag,
					'link'  => 'url',
					//$this->generateUrl('blog_show',array('slug' => 'my-blog-post'));
					//Yii::app()->createUrl('post/index', array('tag' => $tag)),
					'label' => Tag::tagToLabel($tag, $em)
				);
			}
		}
		return $this->render('BlogBundle:Post:index.html.twig', array(
			'entities' => $entities,
		));
	}

	public function manageAction()
	{
		$em       = $this->getDoctrine()->getManager();
		$entities = $em->getRepository('BlogBundle:Post')->findAll();

		return $this->render('BlogBundle:Post:manage.html.twig', array(
			'entities' => $entities,
		));
	}

	/**
	 * Creates a new Post entity.
	 *
	 */
	public function createAction(Request $request)
	{
		$entity = new Post();
		$form   = $this->createForm(new PostType(), $entity);
		$form->bind($request);

		if ($form->isValid())
		{
			$em = $this->getDoctrine()->getManager();

			$entity->setCreateTime(time());
			$entity->setAuthor($this->getUser());
			$em->persist($this->getUser());
			$em->persist($entity);
			$em->flush();

			return $this->redirect($this->generateUrl('post_show', array('id' => $entity->getId())));
		}

		return $this->render('BlogBundle:Post:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView(),
		));
	}

	/**
	 * Displays a form to create a new Post entity.
	 *
	 */
	public function newAction()
	{
		$entity = new Post();
		$form   = $this->createForm(new PostType(), $entity);
		return $this->render('BlogBundle:Post:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView(),
		));
	}

	/**
	 * Finds and displays a Post entity.
	 *
	 */
	public
	function showAction($id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('BlogBundle:Post')->find($id);

		if (!$entity)
		{
			throw $this->createNotFoundException('Unable to find Post entity.');
		}
		$entity->tagLinks = array();
		foreach (Tag::string2array($entity->getTags()) as $tag)
		{
			$entity->tagLinks[] = array(
				'tag'   => $tag,
				'link'  => 'url',
				//$this->generateUrl('blog_show',array('slug' => 'my-blog-post'));
				//Yii::app()->createUrl('post/index', array('tag' => $tag)),
				'label' => Tag::tagToLabel($tag, $em)
			);
		}
		$comment = new Comment();
		$form    = $this->createForm(new CommentType(), $comment);

		$deleteForm = $this->createDeleteForm($id);

		return $this->render('BlogBundle:Post:show.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView(),));
	}

	/**
	 * Displays a form to edit an existing Post entity.
	 *
	 */
	public
	function editAction($id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('BlogBundle:Post')->find($id);

		if (!$entity)
		{
			throw $this->createNotFoundException('Unable to find Post entity.');
		}

		$editForm   = $this->createForm(new PostType(), $entity);

		return $this->render('BlogBundle:Post:edit.html.twig', array(
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
		));
	}

	/**
	 * Edits an existing Post entity.
	 *
	 */
	public
	function updateAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('BlogBundle:Post')->find($id);

		if (!$entity)
		{
			throw $this->createNotFoundException('Unable to find Post entity.');
		}

		$editForm   = $this->createForm(new PostType(), $entity);
		$editForm->bind($request);

		if ($editForm->isValid())
		{
			$entity->setUpdateTime(time());
			$em->persist($entity);
			$em->flush();

			return $this->redirect($this->generateUrl('post_show', array('id' => $id)));
		}

		return $this->render('BlogBundle:Post:edit.html.twig', array(
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
		));
	}

	/**
	 * Deletes a Post entity.
	 *
	 */
	public	function deleteAction($id)
	{

		$em     = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('BlogBundle:Post')->find($id);

		if (!$entity)
		{
			throw $this->createNotFoundException('Unable to find Post entity.');
		}

		$em->remove($entity);
		$em->flush();
		return $this->redirect($this->generateUrl('home'));
	}


}
