<?php
namespace Niels\BlogBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblComment
 *
 * @ORM\Table(name="tbl_comment")
 * @ORM\Entity
 */
class Comment
{

	const STATUS_PENDING=1;
	const STATUS_APPROVED=2;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="content", type="text", nullable=false)
	 */
	private $content;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="status", type="integer", nullable=false)
	 */
	private $status;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="create_time", type="integer", nullable=true)
	 */
	private $createTime;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="author", type="string", length=128, nullable=false)
	 */
	private $author;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=128, nullable=false)
	 */
	private $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="url", type="string", length=128, nullable=true)
	 */
	private $url;

	/**
	 * @var \Post
	 *
	 * @ORM\ManyToOne(targetEntity="Post",inversedBy="comments")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="post_id", referencedColumnName="id")
	 * })
	 */
	private $post;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Comment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Comment
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createTime
     *
     * @param integer $createTime
     * @return Comment
     */
    public function setCreateTime($createTime)
    {
        $this->createTime = $createTime;

        return $this;
    }

    /**
     * Get createTime
     *
     * @return integer 
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return Comment
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Comment
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Comment
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set post
     *
     * @param \Niels\BlogBundle\Entity\Post $post
     * @return Comment
     */
    public function setPost(\Niels\BlogBundle\Entity\Post $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \Niels\BlogBundle\Entity\Post 
     */
    public function getPost()
    {
        return $this->post;
    }
	public function isApproved(){
		return $this->getStatus()==self::STATUS_APPROVED;
	}
}
