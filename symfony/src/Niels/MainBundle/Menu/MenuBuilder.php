<?php
namespace Niels\MainBundle\Menu;
use Doctrine\ORM\EntityManager;
use Mopa\Bundle\BootstrapBundle\Navbar\AbstractNavbarMenuBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\Container;

class MenuBuilder extends AbstractNavbarMenuBuilder
{

	public function createMainMenu(Container $securityContainer)
	{
		$menu = $this->createNavbarMenuItem();
		$menu->addChild('Home', array('route' => 'home'));
		$menu->addChild('About', array('route' => 'about'));
		$menu->addChild('Contact', array('route' => 'contact'));
		$securityContext = $securityContainer->get('security.context');
		if ($securityContext->isGranted('ROLE_USER'))
		{
			$menu->addChild('Logout', array('route' => 'logout'));
		}
		else
		{
			$menu->addChild('Login', array('route' => 'login'));
		}
		return $menu;
	}

	public function createUserMenu(Container $securityContainer)
	{
		$menu            = $this->createNavbarMenuItem();
		$securityContext = $securityContainer->get('security.context');
		if ($securityContext->isGranted('ROLE_USER'))
		{
			$menu->addChild('Create new post', array('route' => 'post_new'));
			$menu->addChild('Manage posts', array('route' => 'post_manage'));
			$menu->addChild('Approve Comments', array('route' => 'comment_list'));
			$menu->addChild('Logout', array('route' => 'logout'));
		}
		return $menu;
	}

	public function createRecentCommentsMenu(EntityManager $em)
	{
		$menu = $this->createNavbarMenuItem();

		$comments = $em->getRepository('BlogBundle:Comment')->findAll();
		foreach ($comments as $comment)
		{

			$menu->addChild($comment->getAuthor() . ' on ' . $comment->getPost()->getTitle(), array(
				'route'           => 'post_show',
				'routeParameters' => array('id' => ($comment->getPost()->getId()))));
		}
		return $menu;
	}

	public function createBreadcrumbMenu(Request $request, EntityManager $em)
	{
		$menu = $this->factory->createItem('root');
		$menu->addChild('Home', array('route' => 'home'));
		switch ($request->get('_route'))
		{
			case 'about':
				$menu
					->addChild('About')
					->setCurrent(TRUE);
				break;
			case 'contact':
				$menu
					->addChild('Contact')
					->setCurrent(TRUE);
				break;
			case 'comment_new':
				$menu
					->addChild('Create comment')
					->setCurrent(TRUE);
				break;
			case 'post_new':
				$menu
					->addChild('Create post')
					->setCurrent(TRUE);
				break;
			case 'post_manage':
				$menu
					->addChild('Manage posts')
					->setCurrent(TRUE);
				break;
			case 'post_edit':
				$menu
					->addChild('Edit post')
					->setCurrent(TRUE);
				break;
			case 'comment_list':
				$menu
					->addChild('Approve Comments')
					->setCurrent(TRUE);
				break;
			case 'login':
				$menu
					->addChild('Login')
					->setCurrent(TRUE);
				break;
			default:
				$post_id = $request->attributes->getInt('id');
				$post    = $em->getRepository('BlogBundle:Post')->find($post_id);
				if ($post)
				{
					$menu
						->addChild($post->getTitle())
						->setCurrent(TRUE);
				}
				break;
		}

		return $menu;
	}
}