<?php

namespace Niels\MainBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testAbout()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/about');

        $this->assertTrue($crawler->filter('html:contains("This is the Symfony site")')->count() > 0);
    }
}
