<?php

Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../../dependencies/bootstrap');

return array(
	'basePath'          => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name'              => 'Yii site',

	'defaultController' => 'post',
	// preloading 'log' component
	'preload'           => array('log'),

	'theme'             => 'bootstrap',
	// autoloading model and component classes
	'import'            => array(
		'application.models.*',
		'application.components.*',
	),

	'modules'           => array(
		// uncomment the following to enable the Gii tool

		'gii' => array(
			'class'          => 'system.gii.GiiModule',
			'password'       => 'giipass',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'      => array('127.0.0.1', '::1'),
			'generatorPaths' => array(
				'bootstrap.gii',
			),
		),
	),

	// application components
	'components'        => array(
		'user'         => array(
			// enable cookie-based authentication
			'allowAutoLogin' => TRUE,
		),
		// uncomment the following to enable URLs in path-format

		'urlManager'   => array(
			'urlFormat' => 'path',
			'rules'     => array(
				'post/<id:\d+>/<title:.*?>'     => 'post/view',
				'posts/<tag:.*?>'               => 'post/index',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			),
		),
		// uncomment the following to use a MySQL database

		'db'           => array(
			'connectionString' => 'mysql:host=localhost;dbname=bachelorproef',
			'emulatePrepare'   => TRUE,
			'username'         => 'niels',
			'password'         => 'bachelorproef',
			'charset'          => 'utf8',
			'tablePrefix'      => 'tbl_',
		),

		'errorHandler' => array(
			// use 'site/error' action to display errors
			'errorAction' => 'site/error',
		),
		'log'          => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				array(
					'class'  => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
		'bootstrap'    => array(
			'class' => 'bootstrap.components.Bootstrap',
		),
		'cache'        => array(
			'class' => 'CApcCache',
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'            => array(
		// this is used in contact page
		'adminEmail'          => 'nielscor@student.hogent.be',
		'commentNeedApproval' => TRUE
	),
);