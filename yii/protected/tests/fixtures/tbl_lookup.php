<?php
/**
 * lookup.php
 *
 * @copyright 2013 Agile Wings
 */
return array(
	1 => array(
		'id'       => 1,
		'name'     => 'Draft',
		'code'     => 1,
		'type'     => 'PostStatus',
		'position' => 1,
	),
	2 => array(
		'id'       => 2,
		'name'     => 'Published',
		'code'     => 2,
		'type'     => 'PostStatus',
		'position' => 2,
	),
	/*
	array(
		'id'       => 3,
		'name'     => 'Archived',
		'code'     => 3,
		'type'     => 'PostStatus',
		'position' => 3,
	),
	*/
	4 => array(
		'id'       => 4,
		'name'     => 'Pending Approval',
		'code'     => 1,
		'type'     => 'CommentStatus',
		'position' => 1,
	),
	5 => array(
		'id'       => 5,
		'name'     => 'Approved',
		'code'     => 2,
		'type'     => 'CommentStatus',
		'position' => 2,
	)
);