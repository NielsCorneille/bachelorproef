<div class="form wide">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'type'                 => 'horizontal',
	'id'                   => 'comment-form',
	'enableAjaxValidation' => true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo CHtml::errorSummary($model); ?>

		<?php echo $form->textFieldRow($model,'title',array('size'=>80,'maxlength'=>128)); ?>

		<?php echo $form->textAreaRow($model,'content',array(
			'rows'=>10,
			'cols'=>70,
			'hint' => 'You may use <a target="_blank" href="http://daringfireball.net/projects/markdown/syntax">Markdown syntax</a>.'
		)); ?>



	<div class="control-group">

		<?php echo $form->labelEx($model,'tags',array('class'=>'control-label')); ?>
		<div class="controls">
		<?php $this->widget('CAutoComplete', array(
			'model'=>$model,
			'attribute'=>'tags',
			'url'=>array('suggestTags'),
			'multiple'=>true,
			'htmlOptions'=>array('size'=>50),
		)); ?>
		<p class="hint">Please separate different tags with commas.</p>
		<?php echo $form->error($model,'tags'); ?>	</div>
	</div>

		<?php echo $form->dropDownListRow($model,'status',Lookup::items('PostStatus')); ?>

		<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'       => 'primary',
			'label'      => $model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->