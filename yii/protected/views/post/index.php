<?php $this->beginWidget('bootstrap.widgets.TbHeroUnit',array(
	'heading'=>CHtml::encode(Yii::app()->name),
)); ?>

<p>Welcome to the Yii site, part of Niels' thesis. You could also visit the other sites:</p>

   <p><?php $this->widget('bootstrap.widgets.TbButton', array(
		   'type'=>'success',
		   'size'=>'large',
		   'label'=>'Zend site',
		   'url'=>'http://localhost/bachelorproef/zend/public/'
	   )); ?>
	   <?php $this->widget('bootstrap.widgets.TbButton', array(
		   'type'=>'inverse',
		   'size'=>'large',
		   'label'=>'Symfony Site',
		   'url'=>'http://localhost/bachelorproef/symfony/web/app_dev.php/'
	   )); ?></p>

<?php $this->endWidget(); ?>
<?php if(!empty($_GET['tag'])): ?>
<h1>Posts Tagged with <i><?php echo CHtml::encode($_GET['tag']); ?></i></h1>
<?php endif; ?>

<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'template'=>"{items}\n{pager}",
)); ?>
