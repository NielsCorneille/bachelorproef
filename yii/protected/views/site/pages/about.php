<?php
$this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
	'About',
);
?>

<?php if($this->beginCache("aboutpage")) { ?>
<h1>About</h1>

<p>This is the Yii site, part of Niels' thesis.<br/>
Niels Corneille - 3TIN - University College Ghent - 2013</p>

	<?php $this->endCache(); } ?>