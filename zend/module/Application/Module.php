<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Authentication\Storage;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

use Zend\Module\Consumer\AutoloaderProvider,
	Zend\EventManager\StaticEventManager;
use Zend\View\Helper\Navigation\AbstractHelper;

class Module
{
	/**
	 * Init function
	 *
	 * @return void
	 */
	public function init()
	{
		// Attach Event to EventManager
		$events = StaticEventManager::getInstance();
	}

	public function onBootstrap(MvcEvent $e)
	{
		$e->getApplication()->getServiceManager()->get('translator');
		$eventManager        = $e->getApplication()->getEventManager();
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);

		$e->getApplication()->getEventManager()->attach('route', array($this, 'checkAcl'));
	}

	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

	public function getAutoloaderConfig()
	{
		return array(
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
				),
			),
		);
	}

	public function getServiceConfig()
	{
		return array(
			'factories' => array(
				'Application\Model\MyAuthStorage'                  => function ($sm)
				{
					return new \Application\Model\MyAuthStorage('zend_site');
				},

				'AuthService'                                      => function ($sm)
				{
					//My assumption, you've alredy set dbAdapter
					//and has users table with columns : user_name and pass_word
					//that password hashed with md5
					$dbAdapter          = $sm->get('Zend\Db\Adapter\Adapter');
					$dbTableAuthAdapter = new DbTableAuthAdapter($dbAdapter,
						'tbl_user', 'username', 'password');

					$authService = new AuthenticationService();
					$authService->setAdapter($dbTableAuthAdapter);
					$authService->setStorage($sm->get('Application\Model\MyAuthStorage'));

					return $authService;
				},

				'Application\Controller\Plugin\UserAuthentication' => function ($sm)
				{
					$userAuth = new \Application\Controller\Plugin\UserAuthentication();
					$userAuth->setAuthService($sm->get('AuthService'));
					return $userAuth;
				},

				'Application\Acl\Acl'                              => function ($sm)
				{
					$acl = new \Application\Acl\Acl(include __DIR__ . '/config/acl.config.php');
					AbstractHelper::setDefaultAcl($acl);
					AbstractHelper::setDefaultRole($acl::DEFAULT_ROLE);
					return $acl;
				}

			),
		);
	}

	/**
	 * MVC preDispatch Event
	 *
	 * @param $event
	 *
	 * @return mixed
	 */
	public function checkAcl($event)
	{
		$target = $event->getTarget();
		if ($target instanceof \Zend\Mvc\Application)
		{
			$di = $target->getServiceManager();
		}
		else
		{
			$di = $target->getServiceLocator();
		}
		$auth = $di->get('Application\Event\Authentication');
		return $auth->checkAcl($event);
	}

}
