<?php
return array(
	'acl' => array(
		'roles'     => array(
			'guest'  => NULL,
			'member' => 'guest'
		),
		'resources' => array(
			'allow' => array(
				'Application\Controller\Index'   => array(
					'home'    => 'guest',
					'about'   => 'guest',
					'contact' => 'guest',
				),
				'Application\Controller\Auth'    => array(
					'login'        => 'guest',
					'success'       => 'guest',
					'logout'       => 'member'
				),
				'Application\Controller\Success' => array(
					'all' => 'guest'
				),
				'Blog\Controller\Post'           => array(
					'all' => 'guest'
				),
			),
			'deny'  => array(
				'Application\Controller\Auth' => array(
					'login'        => 'member',
					'logout'       => 'guest'
				),
			),
		)
	)
);