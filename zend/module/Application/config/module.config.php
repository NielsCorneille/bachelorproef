<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Application;
return array(
	'router'          => array(
		'routes' => array(
			'home'        => array(
				'type'    => 'Zend\Mvc\Router\Http\Literal',
				'options' => array(
					'route'    => '/',
					'defaults' => array(
						'controller' => 'Blog\Controller\Post',
						'action'     => 'index',
					),
				),
			),
			'login' => array(
				'type'    => 'Literal',
				'options' => array(
					'route'    => '/login',
					'defaults' => array(
						'__NAMESPACE__' => 'Application\Controller',
						'controller'    => 'Auth',
						'action'        => 'login',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'process' => array(
						'type'    => 'Segment',
						'options' => array(
							'route'    => '/[:action]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
			'logout' => array(
				'type'    => 'Literal',
				'options' => array(
					'route'    => '/logout',
					'defaults' => array(
						'__NAMESPACE__' => 'Application\Controller',
						'controller'    => 'Auth',
						'action'        => 'logout',
					),
				),
				'may_terminate' => true,
			),

			'success' => array(
				'type'    => 'Literal',
				'options' => array(
					'route'    => '/success',
					'defaults' => array(
						'__NAMESPACE__' => 'Application\Controller',
						'controller'    => 'Auth',
						'action'        => 'success',
					),
				),
				'may_terminate' => true,
			),
			// The following is a route to simplify getting started creating
			// new controllers and actions without needing to create a new
			// module. Simply drop new controllers in, and you can access them
			// using the path /application/:controller/:action
			'application' => array(
				'type'          => 'Literal',
				'options'       => array(
					'route'    => '/application',
					'defaults' => array(
						'__NAMESPACE__' => 'Application\Controller',
						'controller'    => 'Index',
						'action'        => 'index',
					),
				),
				'may_terminate' => TRUE,
				'child_routes'  => array(
					'default' => array(
						'type'    => 'Segment',
						'options' => array(
							'route'       => '/[:controller[/:action]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
							),
							'defaults'    => array(),
						),
					),
				),
			),
			'about'       => array(
				'type'    => 'Literal',
				'options' => array(
					'route'    => '/about',
					'defaults' => array(
						'__NAMESPACE__' => 'Application\Controller',
						'controller'    => 'Index',
						'action'        => 'about',
					),
				),
			),
			'contact'     => array(
				'type'    => 'Literal',
				'options' => array(
					'route'    => '/contact',
					'defaults' => array(
						'__NAMESPACE__' => 'Application\Controller',
						'controller'    => 'Index',
						'action'        => 'contact',
					),
				),
			),
		),
	),
	'service_manager' => array(
		'factories' => array(
			'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
		),
	),
	'translator'      => array(
		'locale'                    => 'en_US',
		'translation_file_patterns' => array(
			array(
				'type'     => 'gettext',
				'base_dir' => __DIR__ . '/../language',
				'pattern'  => '%s.mo',
			),
		),
	),
	'controllers'     => array(
		'invokables' => array(
			'Application\Controller\Index'   => 'Application\Controller\IndexController',
			'Application\Controller\Auth'    => 'Application\Controller\AuthController',
			'Application\Controller\Success' => 'Application\Controller\SuccessController'
		),
	),
	'view_manager'    => array(
		'display_not_found_reason' => TRUE,
		'display_exceptions'       => TRUE,
		'doctype'                  => 'HTML5',
		'not_found_template'       => 'error/404',
		'exception_template'       => 'error/index',
		'template_map'             => array(
			'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
			'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
			'error/404'               => __DIR__ . '/../view/error/404.phtml',
			'error/index'             => __DIR__ . '/../view/error/index.phtml',
		),
		'template_path_stack'      => array(
			__DIR__ . '/../view',
		),
	),

	'di' => array(
		'instance' => array(
			'Application\Event\Authentication' => array(
				'parameters' => array(
					'userAuthenticationPlugin' => 'Application\Controller\Plugin\UserAuthentication',
					'aclClass'                 => 'Application\Acl\Acl'
				)

			),
		)
	),
	'doctrine'     => array(
		'driver' => array(
			__NAMESPACE__ . '_driver' => array(
				'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
				'cache' => 'array',
				'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Model')
			),

			'orm_default'          => array(
				'drivers' => array(
					__NAMESPACE__ . '\Model' =>  __NAMESPACE__ . '_driver'
				)
			))),
);
