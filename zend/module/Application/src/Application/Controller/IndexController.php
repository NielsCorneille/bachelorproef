<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Model\ContactFormFilter;
use Application\Form\ContactForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mail;

class IndexController extends AbstractActionController
{
	public function indexAction()
	{
		return new ViewModel();
	}

	public function contactAction()
	{
		$form     = new ContactForm();
		$request  = $this->getRequest();
		$messages = array();
		$success  = FALSE;
		if ($request->isPost())
		{
			$form->setData($request->getPost());
			// Validate the form
			if ($form->isValid())
			{

				$name    = $form->get('name')->getValue();
				$email   = $form->get('email')->getValue();
				$subject = $form->get('subject')->getValue();
				$message = $form->get('message')->getValue();

				$mail = new Mail\Message();
				$mail->setBody($message);
				$mail->setFrom($email, $name);
				$to     = 'nielscor@hotmail.com';
				$config = $this->getServiceLocator()->get('Config');
				$mail->addTo($config['vars']['emailTo'], $config['vars']['nameTo']);
				$mail->setSubject($subject);
				/*
				$options = new Mail\Transport\SmtpOptions(array(
					'name' => 'localhost',
					'host' => 'smtp.gmail.com',
					'port'=> 587,
					'connection_class' => 'login',
					'connection_config' => array(
						'username' => 'nielscor',
						'password' => 'qsdfsfsf',
						'ssl'=> 'tls',
					),
				));
				$transport = new Mail\Transport\Smtp($options);
				*/
				$transport = new Mail\Transport\Sendmail();
				$transport->send($mail);

				$success = TRUE;

			}
			else
			{
				$messages = $form->getMessages();
			}
		}
		return array(
			'form'     => $form,
			'messages' => $messages,
			'success'  => $success,

		);
	}

	public function aboutAction()
	{
		return new ViewModel();
	}
}
