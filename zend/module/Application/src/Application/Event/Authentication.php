<?php
/**
 * File for Event Class
 *
 * @category  User
 * @package   User_Event
 * @author    Marco Neumann <webcoder_at_binware_dot_org>
 * @copyright Copyright (c) 2011, Marco Neumann
 * @license   http://binware.org/license/index/type:new-bsd New BSD License
 */

/**
 * @namespace
 */
namespace Application\Event;

/**
 * @uses Zend\Mvc\MvcEvent
 * @uses User\Controller\Plugin\UserAuthentication
 * @uses User\Acl\Acl
 */
use Zend\Mvc\MvcEvent as MvcEvent,
	Application\Controller\Plugin\UserAuthentication as AuthPlugin,
	Application\Acl\Acl as AclClass;

use Zend\View\Helper\Navigation\AbstractHelper;

/**
 * Authentication Event Handler Class
 *
 * This Event Handles Authentication
 *
 * @category  User
 * @package   User_Event
 * @copyright Copyright (c) 2011, Marco Neumann
 * @license   http://binware.org/license/index/type:new-bsd New BSD License
 */
class Authentication
{
	/**
	 * @var AuthPlugin
	 */
	protected $_userAuth = null;

	/**
	 * @var AclClass
	 */
	protected $_aclClass = null;

	/**
	 * preDispatch Event Handler
	 *
	 * @param \Zend\Mvc\MvcEvent $event
	 * @throws \Exception
	 */
	public function checkAcl(MvcEvent $event)
	{
		$userAuth = $this->getUserAuthenticationPlugin();
		$acl = $this->getAclClass();
		$role = AclClass::DEFAULT_ROLE;

		if ($userAuth->hasIdentity()) {
			$user = $userAuth->getIdentity();
			$role = 'member';
		}
		AbstractHelper::setDefaultRole($role);

		$routeMatch = $event->getRouteMatch();
		$controller = $routeMatch->getParam('controller');
		$action     = $routeMatch->getParam('action');

		if (!$acl->hasResource($controller)) {
			throw new \Exception('Resource ' . $controller . ' not defined');
		}
		if (!$acl->isAllowed($role, $controller, $action)) {
			$url = $event->getRouter()->assemble(array(), array('name' => 'login'));
			if($action =='login'){
				$url = $event->getRouter()->assemble(array(), array('name' => 'home'));
			}
			$response = $event->getResponse();
			$response->getHeaders()->addHeaderLine('Location', $url);
			$response->setStatusCode(302);
			$response->sendHeaders();
			exit;
		}

		$event->getViewModel()->acl = $acl;
	}

	/**
	 * Sets Authentication Plugin
	 *
	 * @param \User\Controller\Plugin\UserAuthentication $userAuthenticationPlugin
	 * @return Authentication
	 */
	public function setUserAuthenticationPlugin(AuthPlugin $userAuthenticationPlugin)
	{
		$this->_userAuth = $userAuthenticationPlugin;

		return $this;
	}

	/**
	 * Gets Authentication Plugin
	 *
	 * @return \User\Controller\Plugin\UserAuthentication
	 */
	public function getUserAuthenticationPlugin()
	{
		if ($this->_userAuth === null) {
			$this->_userAuth = new AuthPlugin();
		}

		return $this->_userAuth;
	}

	/**
	 * Sets ACL Class
	 *
	 * @param \User\Acl\Acl $aclClass
	 * @return Authentication
	 */
	public function setAclClass(AclClass $aclClass)
	{
		$this->_aclClass = $aclClass;

		return $this;
	}

	/**
	 * Gets ACL Class
	 *
	 * @return \User\Acl\Acl
	 */
	public function getAclClass()
	{
		if ($this->_aclClass === null) {
			$this->_aclClass = new AclClass(array());
		}

		return $this->_aclClass;
	}
}