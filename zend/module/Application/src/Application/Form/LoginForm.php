<?php
namespace Application\Form;

use Zend\Form\Form;

class LoginForm extends Form
{
	public function __construct($name = NULL)
	{
		// we want to ignore the name passed
		parent::__construct('login');
		$this->setAttribute('method', 'post');

		$this->add(array(
			'name'    => 'username',
			'type'    => 'Text',
			'options' => array(
				'label' => 'Username',

			),
		));
		$this->add(array(
			'name'    => 'password',
			'type'          => 'Password',
			'options' => array(
				'label'    => 'Password',
				'required' => TRUE,
				'description' => 'Hint: You may login with demo/demo or admin/admin'
			),
		));
		$this->add(array(
			'name'    => 'rememberme',
			'type' => 'Zend\Form\Element\Checkbox',
			'options' => array(
				'label' => 'Remember me',
			)
		));
		$this->add(array(
			'name'       => 'login',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Login',
			),
		));
	}

}
