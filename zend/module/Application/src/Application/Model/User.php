<?php
/**
 * User.php
 *
 */
//module/Application/src/Application/Model/User.php
namespace Application\Model;


use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblUser
 *
 * @ORM\Table(name="tbl_user")
 * @ORM\Entity
 */
class User
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="username", type="string", length=128, nullable=false)
	 */
	private $username;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="password", type="string", length=128, nullable=false)
	 */
	private $password;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=128, nullable=false)
	 */
	private $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="profile", type="text", nullable=true)
	 */
	private $profile;


	public $rememberme;

	protected $inputFilter;

	public function exchangeArray($data)
	{
		$this->username   = (isset($data['username'])) ? $data['username'] : NULL;
		$this->password   = (isset($data['password'])) ? $data['password'] : NULL;
		$this->rememberme = (isset($data['rememberme'])) ? $data['rememberme'] : NULL;
	}

	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}

	public function getInputFilter()
	{
		if (!$this->inputFilter)
		{
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();

			$inputFilter->add($factory->createInput(array(
				'name'     => 'username',
				'required' => TRUE,
				'filters'  => array(
					array('name' => 'StripTags'),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'       => 'password',
				'required'   => TRUE,
				'filters'    => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 100,
						),
					),
				),
			)));


			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}


	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}