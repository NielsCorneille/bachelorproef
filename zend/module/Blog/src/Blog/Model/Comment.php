<?php
namespace Blog\Model;

use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;


/**
 * TblComment
 *
 * @ORM\Table(name="tbl_comment")
 * @ORM\Entity
 */
class Comment
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="content", type="text", nullable=false)
	 */
	private $content;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="status", type="integer", nullable=false)
	 */
	private $status;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="create_time", type="integer", nullable=true)
	 */
	private $createTime;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="author", type="string", length=128, nullable=false)
	 */
	private $author;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=128, nullable=false)
	 */
	private $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="url", type="string", length=128, nullable=true)
	 */
	private $url;

	/**
	 * @var \Post
	 *
	 * @ORM\ManyToOne(targetEntity="Post")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="post_id", referencedColumnName="id")
	 * })
	 */
	private $post;


	protected $inputFilter;

	public function exchangeArray($data)
	{
		$this->id      = (isset($data['id'])) ? $data['id'] : NULL;
		$this->author  = (isset($data['author'])) ? $data['author'] : NULL;
		$this->email   = (isset($data['email'])) ? $data['email'] : NULL;
		$this->content = (isset($data['content'])) ? $data['content'] : NULL;
		$this->create_time = (isset($data['create_time'])) ? $data['create_time'] : NULL;
	}

	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}

	public function getInputFilter()
	{
		if (!$this->inputFilter)
		{
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();

			$inputFilter->add($factory->createInput(array(
				'name'     => 'id',
				'required' => TRUE,
				'filters'  => array(
					array('name' => 'Int'),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'       => 'author',
				'required'   => TRUE,
				'filters'    => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 100,
						),
					),
				),
			)));


			$inputFilter->add($factory->createInput(array(
				'name'       => 'email',
				'required'   => TRUE,
				'filters'    => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 100,
						),
					),
				),
			)));


			$inputFilter->add($factory->createInput(array(
				'name'       => 'content',
				'required'   => TRUE,
				'filters'    => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 300,
						),
					),
				),
			)));

			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}
